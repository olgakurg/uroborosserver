from app.controllers.BaseController import BaseController


class Professor(BaseController):
    """The controller class for getting professor data.

    Note:
        Implements abstract methods of the base controller class.

    Methods:
        add(entity: dict):
            Adds an entity;
        get_all(professor: str):
            Extracts information from the table by professor surname;
        get_day(professor: str, day: str):
            Extracts information from the table by professor surname and day of the week;
        get_groups(professor: str):
            Extracts information about groups, where given professor teach,
            from a table by professor surname.
    """

    def add(self, entity: dict):
        """Method for adding an entity.

        Behaviour:
            Creates an add request and sends it.

        Arguments are
         * entity(dict): Entity.
        """
        # Forming a request to add an entity
        query = """
        INSERT INTO scheduledb.professors (Department_id, Surname, Name, Patronymic)
        VALUES({{Department_id}},{{Surname}},{{Name}},{{Patronymic}})
       """
        # Sending a query
        self.send_query(query, entity, "edit")

    def get_all(self, professor: str) -> dict:
        """Method for extracting information from a table by professor surname.

        Behaviour:
            Based on the received name of the professor creates
         a query and sends it.

        Arguments are
         * professor: Professor surname.

        Returns:
            result: Received information from the database.
        """

        data = {
            "professor": professor
        }
        print(professor)
        # Forming a query
        query = """SELECT schedule.Parity_of_week,
                sub.Subject_name,
                p.Surname, Name, Patronymic,
                aud.Housing, Floor, Cabinet,
                g.Year_of_study, Group_name
                 FROM schedule
                        join auditorium aud on aud.id=schedule.Auditorium_id
                        join professors p on p.id=schedule.Professor_id
                        join subjects sub on sub.id=schedule.Subject_id
                        join `groups` g on schedule.Group_id = g.id
                        join lesson_time lt on lt.Number=schedule.Lesson_number
                WHERE p.surname={{professor}}"""
        # Sending a query and return the result
        result = self.send_query(query, data)
        return result

    def get_day(self, professor: str, day: str) -> dict:
        """Method for extracting information from a table by professor
         surname and day of the week.

        Behaviour:
            Based on the received name of the professor and the day of the week,
         creates a query and sends it.

        Arguments are
         * professor: Professor surname;
         * day: Day of the week.

        Returns:
            result: Result of sending the request.
        """

        # Setting the request parameters.
        data = {
            "professor": professor,
            "day": day
        }
        # Forming a query
        query = """SELECT schedule.Parity_of_week,
                sub.Subject_name,
                p.Surname, Name, Patronymic,
                aud.Housing, Floor, Cabinet,
                g.Department_id, Speciality_id, Year_of_study, Group_name
                 FROM schedule
                        join auditorium aud on aud.id=schedule.Auditorium_id
                        join professors p on p.id=schedule.Professor_id
                        join subjects sub on sub.id=schedule.Subject_id
                        join `groups` g on schedule.Group_id = g.id
                        join lesson_time lt on lt.Number=schedule.Lesson_number
                WHERE p.surname={{professor}} and schedule.Day_of_the_week={{day}}"""
        # Sending a query and return the result
        result = self.send_query(query, data)
        return result

    def get_groups(self, professor: str) -> dict:
        """Method for extracting information about groups, where given
         professor teach, from a table by professor surname.

        Behaviour:
            Based on the received name of the professor creates
         a query and receives information from the database.

        Arguments are
         * professor: Professor surname.

        Returns:
            result: Result of sending the request.
        """

        # Setting the request parameters.
        data = {
            "professor": professor
        }
        # Forming a query
        query = """SELECT g.Group_name ,CONCAT( p.surname," ", p.Name," ", p.Patronymic ) Professor
                 FROM schedule
                        join professors p on p.id=schedule.Professor_id
                        join groups g on g.id=schedule.Group_id
                WHERE p.surname={{professor}}"""
        # Sending a query and return the result
        result = self.send_query(query, data)
        return result
