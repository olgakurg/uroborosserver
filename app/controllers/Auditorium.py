from app.controllers.BaseController import BaseController


class Auditorium(BaseController):
    """The controller class for getting auditorium data.

    Note:
        Implements abstract methods of the base controller class.

    Methods:
        add(entity: dict):
            Adds an entity;
        get_all(auditorium: str):
            Extracts information from the table by auditorium name;
        get_day(auditorium: str, day: str):
            Extracts information from the table by auditorium name and day of the week.
    """

    def add(self, entity: dict):
        """Method for adding an entity.

        Behaviour:
            Creates an add request and sends it.

        Arguments are
         * entity(dict): Entity.
        """
        # Forming a request to add an entity
        query = """
                INSERT INTO scheduledb.auditorium (Housing, Floor, Cabinet)
                VALUES ({{Housing}}, {{Floor}}, {{Cabinet}})
               """
        # Sending a query
        self.send_query(query, entity, "edit")

    def get_all(self, auditorium: str) -> dict:
        """Method for extracting information from a table by housing
         and cabinet number.

        Behaviour:
            Based on the received audience name, creates a request and sends it.

        Arguments are
         * auditorium: Auditorium name.

        Returns:
            result: Result of sending the request.
        """

        # Setting the request parameters.
        data = {
            "housing": auditorium[0],
            "cabinet": auditorium[2:]
        }
        # Forming a query
        query = """SELECT schedule.Parity_of_week,
                sub.Subject_name,
                p.Surname, Name, Patronymic,
                aud.Housing, Floor, Cabinet,
                g.Department_id, Speciality_id, Year_of_study, Group_name
                 FROM scheduledb.schedule
                        join scheduledb.auditorium aud on aud.id=schedule.Auditorium_id
                        join scheduledb.professors p on p.id=schedule.Professor_id
                        join scheduledb.subjects sub on sub.id=schedule.Subject_id
                        join scheduledb.`groups` g on schedule.Group_id = g.id
                        join scheduledb.lesson_time lt on lt.Number=schedule.Lesson_number
                WHERE aud.Cabinet={{cabinet}} and aud.Housing={{housing}}"""
        # Sending a query and return the result
        result = self.send_query(query, data)
        return result

    def get_day(self, auditorium: str, day: str) -> dict:
        """Method for extracting information from a table by housing,
         cabinet number and day of the week.

        Behaviour:
            Based on the received auditorium name and the day of the week,
         creates a request and sends it.

        Arguments are
         * auditorium: Auditorium name;
         * day: Day of the week.

        Returns:
            result: Result of sending the request.
        """

        # Setting the request parameters.
        data = {
            "housing": auditorium[0],
            "cabinet": auditorium[2:],
            "day": day
        }
        # Forming a query
        query = """SELECT schedule.Parity_of_week,
                sub.Subject_name,
                p.Surname, Name, Patronymic,
                aud.Housing, Floor, Cabinet,
                g.Department_id, Speciality_id, Year_of_study, Group_name
                 FROM scheduledb.schedule
                        join scheduledb.auditorium aud on aud.id=schedule.Auditorium_id
                        join scheduledb.professors p on p.id=schedule.Professor_id
                        join scheduledb.subjects sub on sub.id=schedule.Subject_id
                        join scheduledb.`groups` g on schedule.Group_id = g.id
                        join scheduledb.lesson_time lt on lt.Number=schedule.Lesson_number
                WHERE aud.Cabinet= {{cabinet}} and aud.Housing= {{housing}} and schedule.Day_of_the_week= {{day}}"""
        # Sending a query and return the result
        result = self.send_query(query, data)
        return result
