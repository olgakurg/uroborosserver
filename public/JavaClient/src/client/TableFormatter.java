package client;

public class TableFormatter {

    /**
     * Method: The resulting json is split for formatted output.
     *
     * @param content - json content
     * @return table - parsed json
     */
    public String[] createPrintableStringFromJson(String content) {
            String[] table;
            /* Splitting the json */
            table = content.split("], \\[");
            /* Formatting data for output */
            for (int i = 0; i < table.length; i++) {
                table[i] = table[i].replaceAll("[\\{\\[\\]\\}\"]", "");
                table[i]=table[i].replaceAll("[a-zA-Z_]{1,}: ", "");
                table[i] = table[i].replaceAll(", ", "_");
            }
            return table;
    }
}
