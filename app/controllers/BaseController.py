from abc import ABC, abstractmethod
from jinjasql import JinjaSql
from ..service.connector import DB_connector
from mysql.connector import Error


class BaseController(ABC):
    # Parameterization of SQL queries
    j = JinjaSql()
    # Creating a connection to the database
    data_base = DB_connector().create_connection()
    """The BaseController class is abstract and is used to work with a database.

    Attributes are
     * data_base: MySqlConnection object.

    Methods:
        execute_query(query, data):
            Edits the database;
        execute_read_query(query, data):
            Reads from the database;
        send_query(template, data, method):
            Sends queries;
        (Abstract) end_work(entity: str):
            Gets all the information from the entity;
        (Abstract) get_all(entity: str, day: str):
             Gets day information from entity;
        (Abstract) add(data: dict):
            Adds an entity.
    """

    def execute_query(self, query, data):
        """Method for editing the database.

        Behaviour:
            Creates a special object that makes requests and
         receives their results (cursor). Makes a query to the database
         using the usual SQL syntax and commits the changes made.

        Exceptions:
            In case of exceptions, throws an Error exception.

        Arguments are
         * query: SQL query;
         * data:  Argument for passing variables to SQL query.
        """

        # Creating a cursor
        cursor = self.data_base.cursor()
        try:
            # Making a query to the database
            cursor.execute(query, data)
            # Saving changes
            self.data_base.commit()
            print("Query executed successfully")
        except Error as e:
            print(f"The error '{e}' occurred")

    def execute_read_query(self, query, data):
        """Method for reading from the database.

        Behaviour:
            Creates a special object that makes requests and receives
         their results (cursor).Makes a query to the database using the
         usual SQL syntax and gets the result of the query made.

        Exceptions:
            In case of exceptions, throws an Error exception.

        Arguments are
         * query: SQL query;
         * data:  Argument for passing variables to SQL query.

        Returns:
            result: The result of the executed query.
        """

        # Creating a cursor
        cursor = self.data_base.cursor()
        result = None
        try:
            # Making a query to the database
            cursor.execute(query, data)
            # Getting the result of executing the query
            result = cursor.fetchall()
            field_names = [i[0] for i in cursor.description]
            # Returning the result
            return [dict(zip(field_names, i)) for i in result]
        except Error as e:
            print(f"The error '{e}' occurred")

    def send_query(self, template, data, method="get"):
        """Method for sending queries.

        Behaviour:
            Using the template and data, generates an SQL query and runs the
         query execution method, depending on its purpose.

        Note:
            query: Parameterized string;
            bind_params: Object OrderedDict.

        Arguments are
         * template: SQL query template;
         * data: Data for SQL query;
         * method: Purpose of the request execution method(get or edit).

        Returns:
            Result of the request execution method.
        """

        query, bind_params = self.j.prepare_query(template, data)
        if method == "get":
            return self.execute_read_query(query, bind_params)
        elif method == "edit":
            print(query, bind_params)
            return self.execute_query(query, bind_params)

    def end_work(self):
        """Method for closing the database connection."""

        self.data_base.close()

    @abstractmethod
    def get_all(self, entity: str) -> dict:
        """An abstract method for getting all the information from an entity.

        Arguments are
         * entity(str): Entity.
        """
        pass

    @abstractmethod
    def get_day(self, entity: str, day: str) -> dict:
        """Abstract method for getting day information from entity.

        Arguments are
         * entity(str): Entity;
         * day(str): The day we receive information about.
        """

        pass

    @abstractmethod
    def add(self, data: dict):
        """Abstract method for adding an data.

        Arguments are
         * data(dict): Data to add.
        """
        pass
