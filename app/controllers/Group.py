from app.controllers.BaseController import BaseController


class Group(BaseController):
    """The controller class for getting group data.

    Note:
        Implements abstract methods of the base controller class.

    Methods:
        add(entity: dict):
            Adds an entity;
        get_all(group: str):
            Extracts information from the table by group name;
        get_day(group: str, day: str):
            Extracts information from the table by group name and day of the week.
    """

    def add(self, entity: dict):
        """Method for adding an entity.

        Behaviour:
            Creates an add request and sends it.

        Arguments are
         * entity(dict): Entity.
        """
        # Forming a request to add an entity
        query = """
               INSERT INTO scheduledb.`groups` (Department_id,Speciality_id,Year_of_study, Group_name)
               VALUES({{Department_id}},{{Speciality_id}},{{Year_of_study}},{{Group_name}})
              """
        # Sending a query
        self.send_query(query, entity, "edit")

    def get_all(self, group: str) -> dict:
        """Method for extracting information from a table by group name.

        Behaviour:
            Based on the received group name, creates a query and sends it.

        Arguments are
         * group: Group name.

        Returns:
            result: Result of sending the request.
        """

        # Setting the request parameters.
        data = {
            "group": group
        }
        # Forming a query
        query = """SELECT schedule.Parity_of_week,
                sub.Subject_name,
                p.Surname, Name, Patronymic,
                aud.Housing, Floor, Cabinet,
                g.Department_id, Speciality_id, Year_of_study, Group_name
                 FROM scheduledb.schedule
                        join scheduledb.auditorium aud on aud.id=schedule.Auditorium_id
                        join scheduledb.professors p on p.id=schedule.Professor_id
                        join scheduledb.subjects sub on sub.id=schedule.Subject_id
                        join scheduledb.`groups` g on schedule.Group_id = g.id
                        join scheduledb.lesson_time lt on lt.Number=schedule.Lesson_number
                WHERE g.Group_name={{group}}"""
        # Sending a query and return the result
        result = self.send_query(query, data)
        return result

    def get_day(self, group: str, day: str) -> dict:
        """Method for extracting information from a table by group name
         and day of the week.

        Behaviour:
            Based on the received group name and the day of the week,
         creates a query and sends it.

        Arguments are
         * group: Group name;
         * day: Day of the week.

        Returns:
            result: Result of sending the request.
        """

        # Setting the request parameters.
        data = {
            "group": group,
            "day": day
        }
        # Forming a query
        query = """SELECT schedule.Day_of_the_week, lt.Lessons_start_time,lt.Lessons_end_time,schedule.Parity_of_week,
                          sub.Subject_name,schedule.Lesson_type,g.Group_name ,CONCAT( p.surname," ", p.Name," ", p.Patronymic ) Professor,
                          aud.Housing,aud.Cabinet
                 FROM scheduledb.schedule
                        join scheduledb.auditorium aud on aud.id=schedule.Auditorium_id
                        join scheduledb.professors p on p.id=schedule.Professor_id
                        join scheduledb.subjects sub on sub.id=schedule.Subject_id
                        join scheduledb.groups g on g.id=schedule.Group_id
                        join scheduledb.lesson_time lt on lt.Number=schedule.Lesson_number
                WHERE g.Group_name={{group}} and schedule.Day_of_the_week={{day}}"""
        # Sending a query and return the result
        result = self.send_query(query, data)
        return result
