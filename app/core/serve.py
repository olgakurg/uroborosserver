from http import HTTPStatus
from urllib.parse import urlparse, parse_qs
from http.server import BaseHTTPRequestHandler
import json

from ..routes.routes import Router


class UroborosServer(BaseHTTPRequestHandler):
    # Creating an object of the Router class
    router = Router()

    """The UroborosServer class is extended by the BaseHTTPRequestHandler
    class and is used for routing requests.

    Note:
        Code 200: The requested page has been detected;
        Code 404: Error 404.

    Methods:
        do_GET():
            Processes GET requests;
        do_POST():
            Processes POST requests;
        query_parsing():
            Parses queries;
        data_parsing(json_data):
            Parses Json data;
        send_json(data):
            Sends Json (Response to the request).
    """

    def do_GET(self):
        # Sending the response code (Code 200)
        self.send_response(200)
        # Sending http headers
        self.send_header("Content-type", "application/json")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        self.query_parsing()
        # Starting the router

    def do_POST(self):
        # Reading the HTTP request body and resetting it ti a file
        data_len = int(self.headers['Content-Length'])
        data = self.rfile.read(data_len)
        # Converting a string in JSON format to a Python object
        json_data = json.loads(data)
        print(self.path)
        # Printing Json data
        print("Got object: {}".format(json_data))
        # Parsing Json data
        self.data_parsing(json_data)
        # Adding the response header to the header buffer and registering the received request.
        self.send_response(HTTPStatus.OK)
        # The server does not intend to send any other headers using this send_response()
        self.end_headers()

    def query_parsing(self):
        """Method for parsing queries.

        Behaviour:
            Decodes the formatted path. Analyzes and parses query strings for
         parameters and their values. Sends Json.

        Note:
            format: url/model/?param1=&param2=

        Exceptions:
            In case of exceptions, throws an Error 404.
        """
        # Controller Models
        models = {
            "/get_all/": lambda controller, param: self.router.get_all(controller, param[0]),
            "/get_day/": lambda controller, param: self.router.get_day(controller, param[0], param[1]),
            "/get_group/": lambda controller, param: self.router.get_groups(controller, param[0], param[1])
        }
        # Decoding the formatted path
        request = urlparse(self.path)
        # Analyzing and parsing the query string for parameters and their values
        print(request.query)
        query = parse_qs(request.query)
        params = list(*query.values())
        # If the parameter is in the controller models
        if request.path in models:
            # Getting answer
            # print(params)
            answer = models[request.path](list(query.keys())[0], params)
            if not answer:
                # Throws a 404 error
                self.send_error(404)
            else:
                # Sending Json
                self.send_json(answer)
        else:
            # Throws a 404 error
            self.send_error(404)

    def data_parsing(self, json_data):
        """Method for parsing Json data.

        Note:
            format : url/model;controller

        Attributes are
         * json_data: Formatted Json data.

        Exceptions:
            In case of exceptions, throws an Error 404.
        """

        # Controller Models
        models = {
            "/add": lambda controller, param: self.router.add(controller, param)
        }
        # Decoding the formatted path
        request = urlparse(self.path)
        if request.path in models:
            models[request.path](request.params, json_data)
        else:
            # Throws a 404 error
            self.send_error(404)

    def send_json(self, data):
        """Method for sending json.

        Behaviour:
            Encodes the dictionary(data) in Json format
         and outputs the result to the browser.

        Arguments are
         * data:  Response to the request.
        """

        # Encoding the dictionary in JSON format
        output_json = json.dumps(data)
        # Output the result to the browser
        self.wfile.write(output_json.encode('utf-8'))

